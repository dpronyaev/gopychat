**Build&Install:**

```
cd src/server

go build

cp server /usr/local/bin


cd src/client

go build

cp client /usr/local/bin

```

**Server:**

Just run it to bind it on port 8000:

```
./server
```

**Client:**

```
./client --server $SERVER --port 8000 --nick $NICKNAME
```


