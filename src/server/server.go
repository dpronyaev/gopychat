package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"time"
)

// version
const (
	Version    = "0.0.1"
	ListenPort = ":8000"
	Delimiter  = "----------------------------------------------"
)

// Server struct
type Server struct {
	Port string
}

// Message struct
type Message struct {
	Nickname  string
	Body      string
	TimeStamp string
}

// Slice of all messages
var allMessages []Message

// PoolOfConns Slice of client connections
var PoolOfConns []net.Conn

// Compose method for Message
func (m Message) encodeJSON() []byte {
	JSONm, err := json.Marshal(m)
	if err != nil {
		log.Println("Failed encode to JSON")
		return nil
	}
	return JSONm
}

func main() {
	var myServer = Server{Port: ":8000"}
	myServer.ListenAndServe()
}

// ListenAndServe method for Server
func (srv *Server) ListenAndServe() error {
	fmt.Println("Welcome to GoPyChat Server")
	fmt.Println("version: ", Version)
	fmt.Println(Delimiter)

	port := srv.Port
	if port == "" {
		port = ":8000"
	}
	listener, err := net.Listen("tcp", srv.Port)
	if err != nil {
		log.Fatal("Error starting listener: ", err)
	}

	for {
		conn, err := listener.Accept()
		func() {
			PoolOfConns = append(PoolOfConns, conn)
			fmt.Println("New client connected", conn)
		}()
		if err != nil {
			log.Fatal("Error accept incoming connection: ", err)
			fmt.Println(Delimiter)
		}
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) error {
	defer func() {
		log.Println("Closing connection from ", conn.RemoteAddr())
		fmt.Println(Delimiter)
		conn.Close()
	}()

	reader := bufio.NewReader(conn)
	scanr := bufio.NewScanner(reader)
	for {
		scanned := scanr.Scan()
		if !scanned {
			if err := scanr.Err(); err != nil {
				log.Println(err, conn.RemoteAddr())
				return err
			}
		}

		currentTime := time.Now().Format("02-Jan-2006 15:04:05")
		if len(scanr.Text()) > 0 {
			currentMessage := decodeJSON(scanr.Bytes())

			// print received message to server CLI
			fmt.Println(currentTime)
			fmt.Println(currentMessage.Nickname, "says:", currentMessage.Body)
			var message = Message{currentMessage.Nickname, currentMessage.Body, currentTime}
			sendAll(message.encodeJSON())
		}
	}
}

// Sends message in JSON format to all connected clients
func sendAll(message []byte) {
	for _, currentConn := range PoolOfConns {
		writer := bufio.NewWriter(currentConn)
		writer.WriteString(string(message) + "\n")
		writer.Flush()
	}
}

// ParseJSON implements parsing JSON byte slise into struct
func decodeJSON(raw []byte) Message {
	var m Message
	err := json.Unmarshal(raw, &m)
	if err != nil {
		log.Println("Unable to parse JSON to struct")
	}
	return m
}
