package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"time"
)

// version
const (
	Version = "0.0.2"
)

// Client struct
type Client struct {
	Address string
	Port    string
}

// Message struct
type Message struct {
	Nickname  string
	Body      string
	TimeStamp string
}

// Compose method for Message
func (m Message) encodeJSON() []byte {
	JSON, err := json.Marshal(m)
	if err != nil {
		log.Println("Failed encode to JSON")
		return nil
	}
	return JSON
}

func main() {
	fmt.Println("Welcome to GoPyChat CLIent")
	fmt.Println("version: ", Version)

	// parse flags from command line
	srv := flag.String("srv", "", "server address")
	port := flag.String("port", "8000", "server port")
	nick := flag.String("nick", "Anonimous", "nickname")
	//help := flag.Bool("help", false, "show this message")
	flag.Parse()

	var myClient = Client{Address: *srv, Port: *port}
	conn, err := net.Dial("tcp", myClient.Address+":"+myClient.Port)
	if err != nil {
		log.Fatal("Unable to establish connection to", myClient.Address+":"+myClient.Port)
	}
	defer func() {
		log.Println("Closing connection to server ", conn.RemoteAddr())
		conn.Close()
	}()
	go writer(conn, *nick)
	reader(conn)
}

func writer(conn net.Conn, nick string) error {
	currentTime := time.Now().Format("02-Jan-2006 15:04:05")
	var helloMessage = Message{"ChatBot", nick + " entered the chat", currentTime}
	sendMessage(conn, helloMessage)

	cliScanr := bufio.NewScanner(os.Stdin)
	for cliScanr.Scan() {
		var message = Message{nick, cliScanr.Text(), currentTime}
		sendMessage(conn, message)
	}
	return nil
}

// Sends message in JSON format server
func sendMessage(conn net.Conn, message Message) {
	writer := bufio.NewWriter(conn)
	writer.WriteString(string(message.encodeJSON()) + "\n")
	writer.Flush()
}

func reader(conn net.Conn) error {
	// read incoming messages
	reader := bufio.NewReader(conn)
	scanr := bufio.NewScanner(reader)
	for {
		scanned := scanr.Scan()
		if !scanned {
			if err := scanr.Err(); err != nil {
				log.Println(err, conn.RemoteAddr())
				return err
			}
		}

		currentTime := time.Now().Format("02-Jan-2006 15:04:05")
		if len(scanr.Text()) > 0 {
			currentMessage := decodeJSON(scanr.Bytes())
			fmt.Println(currentTime)
			fmt.Println(currentMessage.Nickname, "says:", currentMessage.Body)
		}
	}
}

// decodeJSON implements parsing JSON byte slise into struct
func decodeJSON(raw []byte) Message {
	var m Message
	err := json.Unmarshal(raw, &m)
	if err != nil {
		log.Println("Unable to parse JSON to struct")
	}
	return m
}
